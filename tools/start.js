// Export modules...
require('./gfns').usingNamespace(GLOBAL,
  require('./utils'),
  require('./binder'),
  require('./builder'));

console.log('\n\
$$$$$$\\\n\
$$  __$$\\\n\
$$ /  \\__| $$$$$$\\  $$$$$$$\\   $$$$$$\\   $$$$$$\\   $$$$$$$\\\n\
\\$$$$$$\\   \\____$$\\ $$  __$$\\ $$  __$$\\ $$  __$$\\ $$  _____|\n\
 \\____$$\\  $$$$$$$ |$$ |  $$ |$$$$$$$$ |$$ /  $$ |\\$$$$$$\\\n\
 $$\   $$ |$$  __$$ |$$ |  $$ |$$   ____|$$ |  $$ | \\____$$\\\n\
 \\$$$$$$  |\\$$$$$$$ |$$ |  $$ |\\$$$$$$$\ \\$$$$$$  |$$$$$$$  |\n\
  \\______/  \\_______|\\__|  \\__| \\_______| \\______/ \\_______/\n\
\n\
Saneos v3 builder by gfunction Computer Science Laboratory\n');

var makeConfig  = loadConfig('configs/make.config.json'),
    startConfig = loadConfig('configs/start.config.json'),
    hdd         = makeConfig.hdd,
    paths       = makeConfig.paths;

var binder = new Binder();

binder.bind(make,         makeConfig)
      .bind(createImages, hdd)
      .bind(attachImages, hdd)
      .bind(install,      paths, getEsysPartition(hdd))
      .bind(begin)
      .bind(detachImages, hdd)
      .trigger();

function begin(next) {
  start(startConfig);
  process.openStdin().on("keypress", function (chunk, key) {
    if (key && key.name === 'c' && key.ctrl)
      next();
  });
}
