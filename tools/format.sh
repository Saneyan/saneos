#!/bin/sh

COUNT=1

for PART_ARG in $@
do
  PART=(`echo ${PART_ARG} | tr -s ',' ' '`)
  PART_LOOP=${PART[1]}
  PART_FILESYSTEM=${PART[3]}

  COUNT=`expr $CONUT + 1`

  # Format as specific file system
  if [ "${PART_FILESYSTEM}" = "FAT32" ]; then
    mkdosfs -F 32 ${PART_LOOP}
  fi
done

exit 0
