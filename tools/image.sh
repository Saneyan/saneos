#!/bin/sh

BASE=(`echo $2 | tr -s ',' ' '`)
FILE_NAME=${BASE[0]}
COUNT=1

for PART_ARG in ${@:3}
do
  PART=(`echo ${PART_ARG} | tr -s ',' ' '`)
  PART_FIRST_SECTOR=${PART[4]}
  PART_LOOP=${PART[1]}
  PART_MOUNT=${PART[2]}
  PART_SIZE=${PART[6]}

  if [ "$1" = "attach" ]; then
    losetup -v --offset $PART_FIRST_SECTOR --sizelimit $PART_SIZE $PART_LOOP $FILE_NAME
    mkdir $PART_MOUNT
    mount $PART_LOOP $PART_MOUNT
  elif [ "$1" = "detach" ]; then
    umount $PART_LOOP
    rm -rf $PART_MOUNT
    losetup -d $PART_LOOP
  fi
done

exit 0
