require('./gfns').usingNamespace(GLOBAL,
  require('./utils'),
  require('./binder'));

var fs = require('fs'),
    cp = require('child_process');

exports.clean = function (hdds, paths) {
};

exports.start = function (config, callback) {
  var i, hdd, qemu;

  log('>>> Starting QEMU with OVMF...');
  log(config);

  for (i = 0, hdd = []; config.hdd[i]; i++)
    hdd.push('-hd' + String.fromCharCode(0x61 + i), config.hdd[i]);

  qemu = cp.spawn('qemu-system-x86_64', [
    '-bios', config.bios,
    '-cpu',  config.cpu,
    '-m',    config.memory,
    '-k',    config.keymap,
    '-L',    '.'
  ].concat(hdd));

  qemu.stdout.on('data', stdLog);
  qemu.stderr.on('data', stdLog);
  qemu.on('close', callback || function () {});
};

exports.make = function (config, callback) {
  var mk;

  log('>>> Making Saneos v3...');
  log(config);

  mk = cp.spawn('make', ['-f', config.makefile]);

  mk.stdout.on('data', stdLog);
  mk.stderr.on('data', stdLog);
  mk.on('close', callback || function () {});
};

exports.install = function (paths, info, callback) {
  var install, args;

  log('>>> Installing...');
  log(paths);

  args = [info.mount];

  paths.forEach(function (path) {
    args.push(path.source + ',' + path.destination);
  });

  install = cp.spawn(__dirname + '/install.sh', args);

  install.stdout.on('data', stdLog);
  install.stderr.on('data', stdLog);
  install.on('close', callback || function () {});
};

exports.attachImages = function (hdds, callback) {
  var binder = new Binder();
  
  log('>>> Attaching images...');
  log(hdds);

  hdds.forEach(function (hdd) {
    binder.bind(function (next) {
      var image = cp.spawn(__dirname + '/image.sh', ['attach'].concat(convertHddInfo(hdd)));

      image.stdout.on('data', stdLog);
      image.stderr.on('data', stdLog);
      image.on('close', next);
    });
  });

  binder.bind(callback || function () {});
  binder.trigger();
};

exports.detachImages = function (hdds, callback) {
  var binder = new Binder();

  log('>>> Detaching images...');
  log(hdds);

  hdds.forEach(function (hdd) {
    binder.bind(function (next) {
      var image = cp.spawn(__dirname + '/image.sh', ['detach'].concat(convertHddInfo(hdd)));

      image.stdout.on('data', stdLog);
      image.stderr.on('data', stdLog);
      image.on('close', next);
    });
  });

  binder.bind(callback || function () {});
  binder.trigger();
};

var formatImages = exports.formatImages = function (hdds, callback) {
  var binder = new Binder();

  log('>>> Formatting images...');
  log(hdds);

  hdds.forEach(function (hdd) {
    binder.bind(function (next) {
      var format = cp.spawn(__dirname + '/format.sh', convertHddInfo(hdd, { baseInfo: false }));

      format.stdout.on('data', stdLog);
      format.stderr.on('data', stdLog);
      format.on('close', next);
    });
  });

  binder.bind(callback || function () {});
  binder.trigger();
};

var createImages = exports.createImages = function (hdds, callback) {
  var targets = [],
      binder = new Binder();

  log('>>> Creating disks...');
  log(hdds);

  hdds.forEach(function (hdd) {
    if (fs.existsSync(hdd.fileName)) {
      log('PASSED: \'' + hdd.fileName + '\' is existed.');
    } else {
      binder.bind(function (next) {
        var cdisk = cp.spawn(__dirname + '/c_disk.sh', convertHddInfo(hdd));

        cdisk.stdout.on('data', stdLog);
        cdisk.stderr.on('data', stdLog);
        cdisk.on('close', next);
      });

      targets.push(hdd);
    }
  });

  binder.bind(attachImages, targets);
  binder.bind(formatImages, targets);
  binder.bind(detachImages, targets);
  binder.bind(callback || function () {});
  binder.trigger();
};

function log() {
  console.log.apply(console, arguments);
}

function convertHddInfo(hdd, option) {
  var ret = [];

  if (!option || option.baseInfo !== false) {
    ret.push([
      hdd.fileName,
      hdd.sectorSize,
      hdd.sectorCount
    ].join(','));
  }

  hdd.partition.forEach(function (partition) {
    ret.push([
      partition.type,
      partition.loop,
      partition.mount,
      partition.fileSystem,
      partition.firstSector,
      partition.lastSector,
      (partition.lastSector + 1 - partition.firstSector) * hdd.sectorSize // Partition size
    ].join(','));
  });

  return ret;
}
