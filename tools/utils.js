var fs = require('fs');

exports.getEsysPartition = function (hdds) {
  var ep = {};

  hdds.forEach(function (hdd) {
    var hddProp, partProp, hddVal, partVal;

    if (!hdd.hasOwnProperty('esysPartition')) return;

    for (hddProp in hdd) {
      hddVal = hdd[hddProp];

      if (hddProp === 'partition') {
        partVal = hddVal[hdd.esysPartition];
        
        for (partProp in partVal) 
          ep[partProp] = partVal[partProp];

      } else if (hddProp !== 'esysPartition') {
        ep[hddProp] = hddVal;
      }
    }
  });

  return ep;
};

// Specified file must be in JSON format
exports.loadConfig = function (path) {
  var data = fs.readFileSync(path, { encoding: 'UTF-8' });
  return JSON.parse(data);
};

exports.stdLog = function (data) {
  console.log('' + data);
};
