var Binder = exports.Binder = function () {
  this._funcs = [];
};

Binder.prototype.bind = function (func) {
  var args = Array.prototype.slice.call(arguments, 1);

  this._funcs.unshift(function (next) {
    func.apply(func.prototype ? func.prototype.constructor : null, args.concat([next]));
  });

  return this;
};

Binder.prototype.trigger = function (num) {
  var len = num === undefined ? this._funcs.length - 1 : num >= 0 ? num : -1;
  len >= 0 && this._funcs[len](this.trigger.bind(this, len - 1));

  return this;
};
