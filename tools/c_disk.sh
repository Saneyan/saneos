#!/bin/sh

BASE=(`echo $1 | tr -s ',' ' '`)
FILE_NAME=${BASE[0]}
SECTOR_SIZE=${BASE[1]}
SECTOR_COUNT=${BASE[2]}
COUNT=1

dd if=/dev/zero of=$FILE_NAME bs=$SECTOR_SIZE count=$SECTOR_COUNT

for PART_ARG in ${@:2}
do
  PART=(`echo ${PART_ARG} | tr -s ',' ' '`)
  PART_TYPE=${PART[0]}
  PART_FIRST_SECTOR=${PART[4]}
  PART_LAST_SECTOR=${PART[5]}

  expect -c "
    spawn gdisk $FILE_NAME
    expect {
      \"Command*\" {
        send \"o\r\"
      }
    }
    expect {
      \"Proceed*\" {
        send \"Y\r\"
      }
    }
    expect {
      \"Command*\" {
        send \"n\r\"
      }
    }
    expect {
      \"Partition number*\" {
        send \"${COUNT}\r\"
      }
    }
    expect {
      \"First sector*\" {
        send \"${PART_FIRST_SECTOR}\r\"
      }
    }
    expect {
      \"Last*\" {
        send \"${PART_LAST_SECTOR}\r\"
      }
    }
    expect {
      \"Hex*\" {
        send \"${PART_TYPE}\r\"
      }
    }
    expect {
      \"Command*\" {
        send \"p\r\"
      }
    }
    expect {
      \"Command*\" {
        send \"w\r\"
      }
    }
    expect {
      \"Do you*\" {
        send \"Y\r\"
      }
    }
    expect {
      \"Command*\" {
        send \"q\r\"
      }
    }
  "

  COUNT=`expr $CONUT + 1`
done

exit 0
