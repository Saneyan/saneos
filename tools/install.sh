#!/bin/sh

if [ ! -e $1/efi/boot ]; then
  mkdir -p -v $1/efi/boot
fi

echo -e "\n=> Copy files"
for PATH_ARG in ${@:2}
do
  MV_ARGS=(`echo ${PATH_ARG} | tr -s ',' ' '`)

  cp -v ${MV_ARGS[0]} $1${MV_ARGS[1]}
done

find $1

exit 0
