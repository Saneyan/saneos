# Saneos (Code name: Eta Carinae)

```
 $$$$$$\                                                    
 $$  __$$\                                                   
 $$ /  \__| $$$$$$\  $$$$$$$\   $$$$$$\   $$$$$$\   $$$$$$$\ 
 \$$$$$$\   \____$$\ $$  __$$\ $$  __$$\ $$  __$$\ $$  _____|
  \____$$\  $$$$$$$ |$$ |  $$ |$$$$$$$$ |$$ /  $$ |\$$$$$$\  
  $$\   $$ |$$  __$$ |$$ |  $$ |$$   ____|$$ |  $$ | \____$$\ 
  \$$$$$$  |\$$$$$$$ |$$ |  $$ |\$$$$$$$\ \$$$$$$  |$$$$$$$  |
   \______/  \_______|\__|  \__| \_______| \______/ \_______/ 

```

**Multi pseudo kernel based operating system working with UEFI.**

## Requirements

* x86\_64 system
* NodeJS `>= 0.10.20`
* gdisk
* QEMU
* UEFI firmware image

## Installation

Install required packages and put a UEFI firmware image in `saneos` directory. The typical UEFI firmware for QEMU is OVMF. The firmware image `OVMF.fd` is available on [here](http://sourceforge.net/apps/mediawiki/tianocore/index.php?title=OVMF).

## Configuration

**Configure script must be in JSON format**

`configs/make.config.json` can be used to configure Saneos. For example:

```json
{ "makefile": "Makefile",
  "hdd": [{
    "fileName": "ud0.img",
    "sectorSize": "512",
    "sectorCount": "40960",
    "partition": [{
      "type": "ef00",
      "fileSystem": "ext2",
      "firstSector": "2048",
      "lastSector": "4096" }]}]}
```

`configs/start.config.json` can be used to configure starting. For example:

```json
{ "bios":   "OVMF.fd",
  "cpu":    "kvm64",
  "memory": "768",
  "hdd": [ "ud0.img" ]}
```

To prevent overriding image file that already exists, change the value of `override` property to `false` in `tools/start.js`:

```js
  builder.createImages(makeConfig.hdd, { override: false }, function () {
    //...
  });
```

# Usage

If you want to build and start Saneos, use `tools/start.js` script:

`node tools/start`
