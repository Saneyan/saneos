ARCH        = x86_64
EFIROOT     = /usr/local
ELROOT      = $(EFIROOT)/include/efi
INCLUDES    = -I. -I$(ELROOT) -I$(ELROOT)/$(ARCH) -I$(ELROOT)/protocol
LIB         = /usr/lib
EFI_CRT_OBJ = $(LIB)/crt0-efi-x86_64.o
EFI_LDS     = $(LIB)/elf_x86_64_efi.lds
LDFLAGS     = -nostdlib -T $(EFI_LDS) -shared -Bsymbolic -L $(EFIROOT)/lib $(EFI_CRT_OBJ)
LOADLIBS    = -lefi -lgnuefi
CFLAGS      = -fpic

prefix   = /usr/bin/
CC       = $(prefix)gcc
LD       = $(prefix)ld
OBJCOPY  = $(prefix)objcopy

%.efi: %.so
	$(OBJCOPY) -j .text -j .sdata -j .data -j .dynamic -j .dynsym -j .rel -j .rela -j .reloc --target=efi-app-x86_64 $*.so $@

%.so: %.o
	$(LD) $(LDFLAGS) $< -o $@ $(LOADLIBS)

%.o: %.c Makefile
	$(CC) $(INCLUDES) $(CFLAGS) -c $< -o $@

TARGETS = boot/bootx64.efi

all: $(TARGETS)

clean: rm -f $(TARGETS)
